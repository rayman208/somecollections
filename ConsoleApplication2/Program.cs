﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        class Country
        {
            public string Name { set; get; }
            public int Population { set; get; }
        }

        static void Main(string[] args)
        {
            List<Country> list = new List<Country>();
            list.Add(new Country() {Name = "Russia", Population = 146});

            Dictionary<string, int> dictionary = new Dictionary<string, int>();
             
            dictionary.Add("Russia",146);
            dictionary.Add("USA",330);

            foreach (var item in dictionary)
            {
                Console.WriteLine(item.Key+" -- "+item.Value);
            }

            Console.WriteLine(dictionary["Russia"]);

            Console.ReadKey();
        }

    }
}
